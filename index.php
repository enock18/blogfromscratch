<?php

    require_once("functions.php");
    $articles=getArticles();

?>

    <!DOCTYPE html>
<html lang="fr">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blog from Scratch</title>
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

</head>
<body class="bloc-principale" >
    
    <div>


            <header> 

                <img src="images/bfcicon.png" alt="icon blog from scratch">
                  <h1 class="logo">Blog from scratch</h1>


             <div class="balise-select">
                     <select name="lesauteurs" id="lesauteurs" class="select">
                     
                            <?php 
                                foreach($authors as $author){?>
                                <choice>Filter by authors</choice>
                                 <option value=""><?php echo $author["firstname"]." ".$author["lastname"]?> </option>
                             <?php    
                              };
                              
                            ?>

                   
                     </select>

                     <select name="lescategories" id="lescategories" class="select">
                    <?php foreach($categories as $categorie){?>
                            <option value="">
                           <?php echo $categorie["category"]; ?>
                            </option>
                     <?php
                      }
                     ?>
                            
                </select>

             </div>

             </header >   
             
                 
                
         <article class="container">
                      <?php
                                         
                        foreach($articles as $article): 

                        ?>  
                        
                 <div class="boites" >
                         <h1 class="titre">
                                 <?=
                            // le titre du cv
                             $article->title;

                                  ?>
                         <h1>

                         <div> <!-- l'auteur -->

                    
                        </div>

                      <div><!-- le te temps -->
                         <time> publié:
                                <?=
                              
                                 $article->publised_at
                               ?>
                         </time>
                    </div>

                   
                   
                    
                        <p class="contenu-de-bloc">
                             <?php
                                    // le contenu du blog
                                     echo substr($article->content, 0, 250)."....";    
                              ?>
                              <br>
                        </p>
                        <br>
                          <a class="liens-lire-lasuite"href="article.php?id= <?= $article->id?>">Lire la suite</a>
                    
                </div>
                    <?php
            // fin de l'article
                        endforeach;
                     ?>


             </article>

                
         </div>
        
        
             </div>
        
        </div>
        <footer>
        <h2 id="contact">Contactez-nous</h2>
        <form>
          <input placeholder="nom">
          <input placeholder="E-mail">
          <textarea placeholder="Votre message ici..."></textarea>
          <button>envoyer</button>

        </form>
        <div class="deuxiemetrait"></div>
        <div class="copyrighteticon">
             <div class="copyright">
              <span>©copyright emukokomaya(); 2021 </span>
             </div>
             <div class="icons">
              <a href="http://www.twitter.fr"><i class="fa fa-twitter"></i></a>
              <a href="http://www.facebook.fr"><i class="fa fa-facebook"></i></a>
              <a href="http://www.instagram.com"><i class="fa  fa-instagram"></i></a>
                    
        </footer>



</body>
</html>