

<?php
     
     if(!isset($_GET['id']) OR !is_numeric($_GET['id']))
     header("location:index.php");
     else
     {
         extract($_GET);
         $id=strip_tags($id);

         require_once("functions.php");

         $arti=getArticle($id);
     }
     
    

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">


    <title>
        <?= $arti->title?>
    </title>
</head>
<body class="article">
<img src="images/bfcicon.png" alt="icon blog from scratch">
    <h1 class="logo">Blog from scratch</h1> <br>
     <a href="index.php">Retour aux articles</a>
    <h1><?= $arti->title ?></h1>
    <time> <?= $arti->publised_at ?></time>
    <p><?= $arti->content?></p>
     <hr>
</body>
</html>